const Telegraf = require('telegraf')
const https = require('https')
const fetch = require('node-fetch');

const BOT_TOKEN = ""

const bot = new Telegraf(BOT_TOKEN)

// Start Command
bot.start(ctx => ctx.reply('Olá, mundo!'))

// Function to start the bot
function startBot() {
    console.log('Bot is running...');
    bot.launch();
}

bot.command('ajuda', ctx => {
    ctx.reply("Certo. Para cadastrar uma nova pessoa, digite o comando /cadastrar seguido de nome, sobrenome e cpf. Veja o exemplo: \n\n '/cadastrar Joao Santos 123'");
})

async function cadastrarPessoa(ctx) {
    const message = ctx.message.text.split(' ');

    let pessoa = {
        nome: message[1],
        sobrenome: message[2],
        cpf: message[3]
    }

    console.log(pessoa)

    url = "http://localhost:8000/api/pessoa/cadastrar"



    fetch(url, {
        method: 'POST',
        body: JSON.stringify(pessoa),
        headers: {
            'accept': 'application/json',
            'Content-Type': 'application/json'
        }
    })
        .then(res => res.json())
        .then(json => {
            console.log(json)
            if(json.err){
                ctx.reply("Erro ao realizar cadastro!")
            }
            else{
                ctx.reply("Cadastro realizado com sucesso!")
            }
        })
        .catch(err => {
            console.log(err)
            ctx.reply("Erro ao realizar cadastro!")
        })

}

bot.command('cadastrar', ctx => cadastrarPessoa(ctx))


// run startBot function
startBot();