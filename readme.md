# node-telegram-bot

É um aplicativo modelo para criação de BOT no telegram que interage com a API do projeto [node-mcv-webapp](https://gitlab.com/jpmacleure/node-mvc-webapp)

## Instalação

Use o node package manager [npm](https://www.npmjs.com/) para instalar as dependências.


```bash
npm install
```

## Uso

Inicilize a aplicação usando o node.js

```bash
node index.js
```

Você deve configurar o BOT_TOKEN do seu bot criado no [Telegram](https://core.telegram.org/bots/api)